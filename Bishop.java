/*
Bishop class is an instance of Piece

                            METHODS
    Bishop(color): initializes Bishop to be of Type = Bishop and Color = color    
        @Param Color color (color of piece)

    validateMove(move): Checks whether move entered is valid for Bishop
        @Param Move move (move object contains the piece to be moved and where it is moving (TODO: Implement Move class))
        @Return: True if move is valid, false otherwise
*/

public class Bishop extends Piece{
   
   public Bishop(Color color){
      super(color);
      this.type = Type.Bishop;
   }

   //TODO:Implement this method
   public boolean validateMove(){
          return true;
   }



}