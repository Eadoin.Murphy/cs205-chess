/*
Board class defines the chess board:

   Square[][] grid: 2D array of board squres (8x8)
   Board board: board instance
   
                  METHODS
   Board(): initializes 2D array of board squares
     
   initializePieces(): deletes all pieces on the board and resets with all the pieces in their initial spots
   
   getSquare(x,y): returns the board square at (x,y) coordinates
      @Param int x (Y coordinate)
      @Param int y (Y coordinate)
      
   printBoard(): Prints the board with Xs denoting empty spaces
      
   
   */

public class Board{

   private static Square[][] grid = new Square[8][8];
   
   private static Board board = new Board();

   private static Piece[] pieces = new Piece[32];
   
   private Board(){
      for(int x = 0; x<8; x++){
         for(int y = 0; y<8; y++){
            grid[x][y] = new Square(x,y);  
         }
      }
   }
   
   public Square getSquare(int x, int y){
      return grid[x][y];
   }
   
   public void printBoard(){

      for(Square r[] : grid){
         for(Square i : r){
            if(i.getPiece() == null)
               System.out.print("|");
            else
                System.out.print(i.getPiece().printPiece());
            System.out.print("\t");
         }
         System.out.println();
      }
   }
   public void initializeBoard(){
        Piece.Type t = Piece.Type.Pawn;
        Piece.Color c = Piece.Color.Black;
        
        Pawn p = new Pawn(c);

        /*Piece pawn = new Piece(t,c);
        Piece rook = new Piece(Piece.Type.Rook,c);
        Piece bishop = new Piece(Piece.Type.Bishop,c);
        Piece knight = new Piece(Piece.Type.Knight,c);
        Piece king = new Piece(Piece.Type.King,c);
        Piece queen = new Piece(Piece.Type.Queen,c);*/
        
        //Black Pawns
        for(int i =0; i<8; i++){
            board.getSquare(1,i).setPiece(new Pawn(c));
            board.getSquare(1,i).getPiece().setSquare(board.getSquare(1,i));
        }


       
        t = Piece.Type.Rook;
        p = new Pawn(c);
      
        //black rooks
        board.getSquare(0,7).setPiece(new Rook(c));
        board.getSquare(0,7).getPiece().setSquare(board.getSquare(0,7));

        board.getSquare(0,0).setPiece(new Rook(c));
        board.getSquare(0,0).getPiece().setSquare(board.getSquare(0,0));
        
        //black knights
        board.getSquare(0,6).setPiece(new Knight(c));
        board.getSquare(0,6).getPiece().setSquare(board.getSquare(0,6));

        board.getSquare(0,1).setPiece(new Knight(c));
        board.getSquare(0,1).getPiece().setSquare(board.getSquare(0,1));
        
        //black bishops
        board.getSquare(0,5).setPiece(new Bishop(c));
        board.getSquare(0,5).getPiece().setSquare(board.getSquare(0,5));

        board.getSquare(0,2).setPiece(new Bishop(c));
        board.getSquare(0,2).getPiece().setSquare(board.getSquare(0,2));
        
        //black king and queen       
        board.getSquare(0,4).setPiece(new King(c));
        board.getSquare(0,4).getPiece().setSquare(board.getSquare(0,4));

        board.getSquare(0,3).setPiece(new Queen(c));
        board.getSquare(0,3).getPiece().setSquare(board.getSquare(0,3));
        
        //white pieces
        c = Piece.Color.White;
        
        
        //white pawns
        
        for(int i =0; i<8; i++){
            board.getSquare(6,i).setPiece(new Pawn(c));
            board.getSquare(6,i).getPiece().setSquare(board.getSquare(6,i));
        }
            
        //white rooks

        board.getSquare(7,7).setPiece(new Rook(c));
        board.getSquare(7,7).getPiece().setSquare(board.getSquare(7,7));

        board.getSquare(7,0).setPiece(new Rook(c));
        board.getSquare(7,0).getPiece().setSquare(board.getSquare(7,0));
        
        //white knights
        board.getSquare(7,6).setPiece(new Knight(c));
        board.getSquare(7,6).getPiece().setSquare(board.getSquare(7,7));

        board.getSquare(7,1).setPiece(new Knight(c));
        board.getSquare(7,1).getPiece().setSquare(board.getSquare(7,1));
        
        //white bishops
        board.getSquare(7,5).setPiece(new Bishop(c));
        board.getSquare(7,5).getPiece().setSquare(board.getSquare(7,5));

        board.getSquare(7,2).setPiece(new Bishop(c));
        board.getSquare(7,2).getPiece().setSquare(board.getSquare(7,2));
        
        //white king and queen       
        board.getSquare(7,4).setPiece(new King(c));
        board.getSquare(7,4).getPiece().setSquare(board.getSquare(7,4));

        board.getSquare(7,3).setPiece(new Queen(c));
        board.getSquare(7,3).getPiece().setSquare(board.getSquare(7,3));

   }
   
   public static void main(String [] args){
      board.initializeBoard();      
      board.printBoard();
     /* System.out.println("New Board");
      Square s = new Square(4,4);
      board.getSquare(1,0).getPiece().setSquare(s);
      board.getSquare(4,4).setPiece(board.getSquare(1,0).getPiece());
      board.getSquare(1,0).setPiece(null);
      board.printBoard();*/
   }


}