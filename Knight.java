/*
Knight class is an instance of Piece

                            METHODS
    Knight(color): initializes Knight to be of Type = Knight and Color = color    
        @Param Color color (color of piece)

    validateMove(move): Checks whether move entered is valid for Knight
        @Param Move move (move object contains the piece to be moved and where it is moving (TODO: Implement Move class))
        @Return: True if move is valid, false otherwise
*/

public class Knight extends Piece{
   
   public Knight(Color color){
      super(color);
      this.type = Type.Knight;
   }

   //TODO:Implement this method
   public boolean validateMove(){
          return true;
   }



}