/*
Pawn class is an instance of Piece:

    boolean isFirstMove: to be used in validate move method (true if pawn has yet to be moved)

                            METHODS
    Pawn(color): initializes pawn to be of Type = Pawn and Color = color    
        @Param Color color (color of Pawn)

    validateMove(move): Checks whether move entered is valid for Pawn
        @Param Move move (move object contains the piece to be moved and where it is moving (TODO: Implement Move class))
        @Return: True if move is valid, false otherwise
*/

public class Pawn extends Piece{

   private boolean isFirstMove;

   public Pawn(Color color){
      super(color);
      this.type = Type.Pawn;
      this.isFirstMove = true;
   }

   //TODO:Implement this method
   public boolean validateMove(){
        this.isFirstMove = false;
        return true;
   }



}