/*
Rook class is an instance of Piece:

       boolean isFirstMove: to be used in validate move method (true if pawn has yet to be moved)
    
                            METHODS
    Rook(color): initializes rook to be of Type = rook and Color = color    
        @Param Color color (color of piece)

    validateMove(move): Checks whether move entered is valid for rook
        @Param Move move (move object contains the piece to be moved and where it is moving (TODO: Implement Move class))
        @Return: True if move is valid, false otherwise
*/

public class Rook extends Piece{
   
  private boolean isFirstMove;
  public Rook(Color color){
    super(color);
    this.type = Type.Rook;
    this.isFirstMove = true;
  }

  //TODO:Implement this method
  public boolean validateMove(){
    this.isFirstMove = false;
    return true;
  }



}